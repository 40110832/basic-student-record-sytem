﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW
{   
    public class Staff 
    {
        /* Author: Ivaylo Bogdanov
         * Matriculation Number:40110832
         * Class Staff is used to create fields and properties and
         * Methods for staff object created in GUI
         * Date last modified: 24/10/14
        */

        //declaring private fields
        private int staff_id;
        private string first_n;
        private string second_n;
        private string date_of_birth;
        private string department;
        private int hours_pr;
        private int hours_wrk;
        private string month;
        private int year;
     
        //Declare a HPR propery of type Int
        public int StaffID
        {
            get
            {
                return staff_id;
            }
            set
            {
                staff_id = value;
            }
        }
        //Declare a FirstName propery of type String
        public string FirstName
        {
            get
            {
                return first_n;
            }
            set
            {
                first_n = value;
            }
        }

        //Declare a SecondName propery of type String
        public string SecondName
        {
            get
            {
                return second_n; 
            }
            set
            {
                second_n = value;
            }
        }
        //Declare a DateOfBirth propery of type String
        public string DateOfBirth
        {
            get
            {
                return date_of_birth;
            }
            set
            {
                date_of_birth = value;
            }
        }
        //Declare a DEP propery of type String
        public string DEP  //Department
        {
            get
            {
                return department;
            }
            set
            {
                department = value;
            }
        }
        //Declare a HPR propery of type Int
        public int HPR  //HoursPerRate
        {
            get
            {
                return hours_pr ;
            }
            set
            {
                hours_pr = value ;
            }
        }
        //Declare a HW propery of type Int
        public int HW  //HoursWorked
        {
            get
            {
                return hours_wrk;
            }
            set
            {
                hours_wrk = value;
            }
        }
        //Declare a Mth propery of type String
        public string Mth  //Month
        {
            get
            {
                return month;
            }
            set
            {
                month = value;
            }
        }
        //Declare a Year propery of type Int
        public int Year 
        {
            get
            {
                return year;
            }
            set
            {
                year = value;
            }
        }

        public int CalcPay(int hours)
        {
            /* This methods returns the payment(pay)
             * it calculates the pay rate and hours worked 
             * based on the result of calcTaxRate method
             */

            int pay;
            if(calcTaxRate(Convert.ToInt32(hours_pr)) == 10)
            {
                pay = (hours_pr * hours_wrk) * 90 / 100;
                return pay;
            }

            else
            {
                pay = (hours_pr * hours_wrk) * 80 / 100;
                return pay;
            }
        }


        public int calcTaxRate(int grossPayPence)
   
            /* this method returns the tax rate based on the gross payvalue
             * returned tax rate will be either 10 or 20
             * if the gross value is under 1000, reurned value is 10
             * otherwise the returned value is 20
             */
        {
            
            int tx_rate;
           
            if(grossPayPence<1000)
            {
                tx_rate = 10;
            }


            else
            {
                tx_rate = 20;
            }

            return tx_rate;
    
        }
       
    }
}


  