﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CW
{
    
    public partial class GUI :Window
    {
        /* Author: Ivaylo Bogdanov
         * Matriculation Number:40110832
         * Creates the Mainwindow
         * declaring objects - person1 and d1
         * assigning the values of the properites
         * using validation 
         * add events to get,clear and set Methods
         * Date last modified: 24/10/14
         */

        Staff person1; //declaring staff object
        Demonstrator d1; // declaring demonstrator object

        public GUI()
        {
            InitializeComponent();
        }

        private void setbtn_Click(object sender, RoutedEventArgs e)
        {
            //setbtn_Click method is used to assign values to the properties 

            if (st_or_dem.Text == "Staff")
            {

                person1 = new Staff();  //Creating object person

                person1.FirstName = fname.Text;
                person1.SecondName = lname.Text;
                person1.DateOfBirth = dobirth.Text;
                person1.DEP = dep.Text;
                person1.Mth = mth.Text;


                try //Exception Handling Statements
                {
                    person1.StaffID = Int32.Parse(stid.Text);
                }

                catch (FormatException) { }

                try
                {
                    person1.HPR = Int32.Parse(prate.Text);
                }

                catch (FormatException) { }

                try
                {
                    person1.HW = Int32.Parse(hw.Text);
                }

                catch (FormatException) { }

                try
                {
                    person1.Year = Int32.Parse(yr.Text);
                }

                catch (FormatException) { }

                //Validation
                if (person1.StaffID < 1000 || person1.StaffID > 2000)
                {
                    _new1.Text = ("Please enter a number between 1000 and 2000 in StaffID box ");
                    stid.Clear();
                }
                else
                {
                    _new1.Text = ("");
                }

                if (person1.Year <= 1900 || person1.Year > 1998)
                {
                    _new4.Text = ("Please enter a year between 1900 and 1998");
                    yr.Clear();
                }
                else
                {
                    _new4.Text = ("");
                }

                if (person1.HPR < 1 || person1.HPR > 99999)
                {
                    _new2.Text = ("Please enter your payrate");
                    prate.Clear();
                }
                else
                {
                    _new2.Text = ("");
                }
                if (person1.HW < 1 || person1.HW > 65)
                {
                    _new3.Text = ("Please enter hours(1-65)");
                    hw.Clear();

                }
                else
                {
                    _new3.Text = ("");
                }
            }

            else 
            {
                d1 = new Demonstrator();  //Creating object person

                d1.FirstName = fname.Text;
                d1.SecondName = lname.Text;
                d1.DateOfBirth = dobirth.Text;
                d1.DEP = dep.Text;
                d1.Mth = mth.Text;
                d1.Course = cs.Text;
                d1.Supervisor = sp.Text;
         


                try //Exception Handling Statements
                {
                    d1.StaffID = Int32.Parse(stid.Text);
                }

                catch (FormatException) { }

                try
                {
                    d1.HPR = Int32.Parse(prate.Text);
                }

                catch (FormatException) { }

                try
                {
                    d1.HW = Int32.Parse(hw.Text);
                }

                catch (FormatException) { }

                try
                {
                    d1.Year = Int32.Parse(yr.Text);
                }

                catch (FormatException) { }

                //Validation
                if (d1.StaffID < 1000 || d1.StaffID > 2000)
                {
                    _new1.Text = ("Please enter a number between 1000 and 2000 in StaffID box ");
                    stid.Clear();
                }
                else
                {
                    _new1.Text = ("");
                }

                if (d1.Year <= 1900 || d1.Year > 1998)
                {
                    _new4.Text = ("Please enter a year between 1900 and 1998");
                    yr.Clear();
                }
                else
                {
                    _new4.Text = ("");
                }

                if (d1.HPR < 1 || d1.HPR > 99999)
                {
                    _new2.Text = ("Please enter your payrate");
                    prate.Clear();
                }
                else
                {
                    _new2.Text = ("");
                }
                if (d1.HW < 1 || d1.HW > 65)
                {
                    _new3.Text = ("Please enter hours(1-65)");
                    hw.Clear();
                }
                else
                {
                    _new3.Text = ("");
                }
            }

        }
        private void setbtn_MouseEnter(object sender, MouseEventArgs e)
        {
           ValMethod();
        }
      
        private void calcpaybtn_MouseEnter(object sender, MouseEventArgs e)
        {
            ValMethod();
           
       
        }

        private void ValMethod() // Creating Method For Validation if the fields are empty           
            
    {
        if (st_or_dem.Text == "Staff")
        {
            if (fname.Text == "" || lname.Text == "" || dobirth.Text == "" ||
               dep.Text == "" || stid.Text == "" || prate.Text == "" || hw.Text == "" ||
               mth.Text == "" || yr.Text == "" || st_or_dem.Text=="")
            {
                MessageBox.Show("Please fill all empty fields before clicking this button!");
            }
        }
        else if (st_or_dem.Text == "Demonstrator")
        {
            if(fname.Text == "" || lname.Text == "" || dobirth.Text == "" ||
               dep.Text == "" || stid.Text == "" || prate.Text == "" || hw.Text == "" ||
               mth.Text == "" || yr.Text == "" || cs.Text =="" || sp.Text =="")
            {
                MessageBox.Show("Please fill all empty fields before clicking this button!");
            }
        }
        else
        {
              if ( st_or_dem.Text=="")
                   {
                       MessageBox.Show("Please fill all empty fields before clicking this button!");
                                       
              }
        }
    }
        private void clearbtn_Click(object sender, RoutedEventArgs e)  
        {
            // clearbtn_Click Method is used to delete the content of textboxes and comboboxes
            // when clear button is pressed
            fname.Clear();
            lname.Clear();
            dobirth.Text = String.Empty;
            dep.Clear();
            stid.Clear();
            prate.Clear();
            hw.Clear();
            yr.Clear();
            mth.Text=String.Empty;
            cs.Clear();
            sp.Clear();
           
        }

        private void getbtn_Click(object sender, RoutedEventArgs e)
        {
            // getbtn_Clic Method is used to get the values of textboxes and comboboxes
            // when get button is pressed
            if (st_or_dem.Text == "Staff")
            {

                try
                {
                    fname.Text = person1.FirstName;
                    lname.Text = person1.SecondName;
                    dobirth.Text = person1.DateOfBirth;
                    dep.Text = person1.DEP;
                    mth.Text = person1.Mth;
                    stid.Text = Convert.ToString(person1.StaffID);
                    prate.Text = Convert.ToString(person1.HPR);
                    hw.Text = Convert.ToString(person1.HW);
                    yr.Text = Convert.ToString(person1.Year);
                }

                catch (NullReferenceException) { }//Exception Handling Statements   
            }

            else
            {

                try
                {
                    fname.Text = d1.FirstName;
                    lname.Text = d1.SecondName;
                    dobirth.Text = d1.DateOfBirth;
                    dep.Text = d1.DEP;
                    mth.Text = d1.Mth;
                    stid.Text = Convert.ToString(d1.StaffID);
                    prate.Text = Convert.ToString(d1.HPR);
                    hw.Text = Convert.ToString(d1.HW);
                    yr.Text = Convert.ToString(d1.Year);
                    cs.Text = d1.Course;
                    sp.Text = d1.Supervisor;                  
                }

                catch (NullReferenceException) { }//Exception Handling Statements   
            }
        }

        private void calcpaybtn_Click(object sender, RoutedEventArgs e)
        {
            //calcpaybtn_Click method creates new window 
            //if staff is chosen in combobox - payslip win will be shown
            //else pasyslip2 win will be shown

            if (st_or_dem.Text == "Staff")
            {
                try
                {
                    payslip newWin = new payslip();
                    newWin.Show();
                    newWin.fname_txt.Text = person1.FirstName;
                    newWin.lname_txt.Text = person1.SecondName;
                    newWin.dep_txt.Text = person1.DEP;
                    newWin.hw_txt.Text = Convert.ToString(person1.HW);
                    newWin.gp_txt.Text = Convert.ToString(person1.HW * person1.HPR);
                    newWin.tr_txt.Text = Convert.ToString(person1.calcTaxRate(person1.HW * person1.HPR));
                    newWin.np_txt.Text = Convert.ToString(person1.CalcPay(person1.HW));
                }
                catch (NullReferenceException) { }//Exception Handling Statements   
            }
            else
            {
                try
                {
                    payslip2 newWin = new payslip2();
                    newWin.Show();
                    newWin.fname_txt.Text = d1.FirstName;
                    newWin.lname_txt.Text = d1.SecondName;
                    newWin.dep_txt.Text = d1.DEP;
                    newWin.cs_txt.Text = d1.Course;
                    newWin.sp_txt.Text = d1.Supervisor;
                    newWin.hw_txt.Text = Convert.ToString(d1.HW);
                    newWin.gp_txt.Text = Convert.ToString(d1.HW * d1.HPR);
                    newWin.tr_txt.Text = "5";
                    newWin.np_txt.Text = Convert.ToString((d1.HW * d1.HPR) * 0.95);
                }
                catch (NullReferenceException) { }//Exception Handling Statements 
            }
        }
   
        private void st_or_dem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if (st_or_dem.Text == "Demonstrator")
            {
                 VisibilityHidden();
            }

            else if (st_or_dem.Text == "Staff")
            {
                cs.Visibility = System.Windows.Visibility.Visible;
                sp.Visibility = System.Windows.Visibility.Visible;
                cou.Visibility = System.Windows.Visibility.Visible;
                sup.Visibility = System.Windows.Visibility.Visible;
            }
            else if (st_or_dem.Text == "")
            {
             VisibilityHidden();
            }
            else
            {
                VisibilityHidden();
            }
        }      
    
        private void VisibilityHidden()
        {
            //void method for visibility
            cs.Visibility = System.Windows.Visibility.Hidden;
            sp.Visibility = System.Windows.Visibility.Hidden;
            cou.Visibility = System.Windows.Visibility.Hidden;
            sup.Visibility = System.Windows.Visibility.Hidden;
        }
    }           
}       





