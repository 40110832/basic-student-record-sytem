﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW
{
    class Demonstrator: Staff
    {

        /* Author: Ivaylo Bogdanov
         * Matriculation Number:40110832
         * Class Staff is used to create fields and properties and
         * Methods for Demonstratot object created in GUI
         * it inherits Staff methods and properties
         * Date last modified: 24/10/14
        */

        private string course;
        private string supervisor;
       
        //Declare a Course propery of type String
        public string Course
        {
            get
            {
                return course;
            }
            set
            {
                course = value;
            }
        }
        //Declare a Supervisor propery of type String
        public string Supervisor
        {
            get
            {
                return supervisor;
            }
            set
            {
                supervisor = value;
            }
        }
    }
}
